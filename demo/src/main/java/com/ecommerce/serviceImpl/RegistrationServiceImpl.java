package com.ecommerce.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.model.Registration;
import com.ecommerce.repository.RegistrationRepository;
import com.ecommerce.service.RegistrationService;

@Service
public class RegistrationServiceImpl implements RegistrationService {
	
	@Autowired
	RegistrationRepository regRepo;

	@Override
	public Registration submitRegistration(Registration registration) {
		
		
		 regRepo.save(registration);
		 return registration;
	}

	@Override
	public List<Registration> getAllRegisteredUsers() {
		// TODO Auto-generated method stub
		return regRepo.findAll();
	}

	@Override
	public Registration getRegisteredUserByPhone(long phone) {
		// TODO Auto-generated method stub
		return regRepo.findByPhone(phone);
	}

}
