package com.ecommerce.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.ecommerce.model.SalesOrder;
import com.ecommerce.repository.OrderRepository;
import com.ecommerce.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService{
	
	@Autowired
	OrderRepository orderrepo;

	@Override
	public SalesOrder createOrder(SalesOrder salesorder) {
		// TODO Auto-generated method stub
		return orderrepo.save(salesorder);
	}

	@Override
	public List<SalesOrder> getAllOrders() {
		// TODO Auto-generated method stub
		return orderrepo.findAll();
	}

	

}
